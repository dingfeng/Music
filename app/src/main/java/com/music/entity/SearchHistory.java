package com.music.entity;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by dingfeng on 2016/9/30.
 */
@DatabaseTable(tableName = "table_search_history")
public class SearchHistory {

    @DatabaseField(generatedId = true, uniqueCombo = true, columnName = "id")
    private int id;
    @DatabaseField(columnName = "keyword", unique = true)
    private String keyword;
    @DatabaseField(columnName = "ctime")
    private long ctime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public long getCtime() {
        return ctime;
    }

    public void setCtime(long ctime) {
        this.ctime = ctime;
    }
}
