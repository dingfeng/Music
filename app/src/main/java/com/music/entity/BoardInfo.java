package com.music.entity;

import com.music.entity.online.OnlineMusicInfo;

import java.util.List;

/**
 * Created by dingfeng on 2016/9/21.
 */
public class BoardInfo {

    private List<OnlineMusicInfo> song_list;
    private Billboard billboard;

    public List<OnlineMusicInfo> getOnlineMusicList() {
        return song_list;
    }

    public void setOnlineMusicList(List<OnlineMusicInfo> list) {
        this.song_list = list;
    }

    public Billboard getBillboard() {
        return billboard;
    }

    public void setBillboard(Billboard billboard) {
        this.billboard = billboard;
    }

    public static class Billboard {
        int billboard_type;// 榜单类型
        String update_date;// 更新时间
        int billboard_songnum;// 歌曲数目
        String name;// 榜单名
        String comment;// 榜单介绍
        String pic_s640;// 榜单图片
        String pic_s444;// 榜单图片
        String pic_s260;// 榜单图片
        String pic_s210;// 榜单图片
        String web_url;// 榜单链接

        public int getBillboardType() {
            return billboard_type;
        }

        public void setBillboard_type(int billboardType) {
            this.billboard_type = billboardType;
        }

        public String getUpdateDate() {
            return update_date;
        }

        public void setUpdateDate(String update_date) {
            this.update_date = update_date;
        }

        public int getBillboardSongnum() {
            return billboard_songnum;
        }

        public void setBillboardSongnum(int billboard_songnum) {
            this.billboard_songnum = billboard_songnum;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getComment() {
            return comment;
        }

        public void setComment(String comment) {
            this.comment = comment;
        }

        public String getPics640() {
            return pic_s640;
        }

        public void setPics640(String pics640) {
            this.pic_s640 = pics640;
        }

        public String getPics444() {
            return pic_s444;
        }

        public void setPics444(String pics444) {
            this.pic_s444 = pics444;
        }

        public String getPics260() {
            return pic_s260;
        }

        public void setPics260(String pics260) {
            this.pic_s260 = pics260;
        }

        public String getPics210() {
            return pic_s210;
        }

        public void setPic_s210(String pics210) {
            this.pic_s210 = pics210;
        }

        public String getWebUrl() {
            return web_url;
        }

        public void setWebUrl(String webUrl) {
            this.web_url = webUrl;
        }

    }
}
