package com.music.entity.online;

import java.io.Serializable;

/**
 * Created by dingfeng on 2016/9/22.
 */
public class OnlineArtistInfo implements Serializable{

    private String name;// 姓名
    private String birth;// 生日
    private String constellation;// 星座
    private String country;// 国籍
    private String avatar_small;// 头像
    private String avatar_s180;
    private String avatar_mini;
    private String avatar_s1000;
    private String avatar_s500;
    private String avatar_big;
    private String avatar_middle;
    private int albums_total;// 专辑数量
    private String url;// 链接
    private String intro;// 简介

    public String getName() {
        return name;
    }

    public String getBirth() {
        return birth;
    }

    public String getConstellation() {
        return constellation;
    }

    public String getCountry() {
        return country;
    }

    public String getAvatar_small() {
        return avatar_small;
    }

    public String getAvatar_s180() {
        return avatar_s180;
    }

    public String getAvatar_mini() {
        return avatar_mini;
    }

    public String getAvatar_s1000() {
        return avatar_s1000;
    }

    public String getAvatar_s500() {
        return avatar_s500;
    }

    public String getAvatar_big() {
        return avatar_big;
    }

    public String getAvatar_middle() {
        return avatar_middle;
    }

    public int getAlbums_total() {
        return albums_total;
    }

    public String getUrl() {
        return url;
    }

    public String getIntro() {
        return intro;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }

    public void setConstellation(String constellation) {
        this.constellation = constellation;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setAvatar_small(String avatar_small) {
        this.avatar_small = avatar_small;
    }

    public void setAvatar_s180(String avatar_s180) {
        this.avatar_s180 = avatar_s180;
    }

    public void setAvatar_mini(String avatar_mini) {
        this.avatar_mini = avatar_mini;
    }

    public void setAvatar_s1000(String avatar_s1000) {
        this.avatar_s1000 = avatar_s1000;
    }

    public void setAvatar_s500(String avatar_s500) {
        this.avatar_s500 = avatar_s500;
    }

    public void setAvatar_big(String avatar_big) {
        this.avatar_big = avatar_big;
    }

    public void setAvatar_middle(String avatar_middle) {
        this.avatar_middle = avatar_middle;
    }

    public void setAlbums_total(int albums_total) {
        this.albums_total = albums_total;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setIntro(String intro) {
        this.intro = intro;
    }

   /*
{
    "firstchar": "W",
    "birth": "1999-09-21",
    "constellation": "处女",
    "weight": "0.00",
    "ting_uid": "10559334",
    "piao_id": "0",
    "avatar_small": "http://musicdata.baidu.com/data2/pic/c7d24c9011941462dd8bbec68a43923d/252758568/252758568.jpg",
    "is_collect": 0,
    "area": "0",
    "listen_num": "7370327",
    "mv_total": 586,
    "avatar_s180": "http://musicdata.baidu.com/data2/pic/d512d7cc58ac0958714b0eeeecad1018/129242625/129242625.jpg",
    "name": "王俊凯",
    "gender": "0",
    "avatar_mini": "http://musicdata.baidu.com/data2/pic/6ddfe7cb5c710292b3ee726dc00c105b/252758693/252758693.jpg",
    "share_num": 0,
    "stature": "0.00",
    "avatar_s1000": "http://musicdata.baidu.com/data2/pic/129242614/129242614.jpg",
    "albums_total": "2",
    "avatar_s500": "http://musicdata.baidu.com/data2/pic/68fb77f2a2a52688ab9654b68d36771f/252758819/252758819.jpg",
    "bloodtype": "5",
    "artist_id": "14947058",
    "avatar_big": "http://musicdata.baidu.com/data2/pic/4a102def6091db52bd3a25c14d6044dc/129242621/129242621.jpg",
    "translatename": "",
    "url": "http://music.baidu.com/artist/10559334",
    "songs_total": "11",
    "avatar_middle": "http://musicdata.baidu.com/data2/pic/029374f1c52e2b5cab1894e9d9e6e249/129242632/129242632.jpg",
    "intro": "王俊凯，1999年9月21日生于重庆，中国歌手。TFBOYS队长。\n2010年底加入TF家族成为练习生，2012年2月翻唱《囚鸟》、7月翻唱《一个像夏天一个像秋天》、9月翻唱《我的歌声里》，2013年4月翻唱《当爱已成往事》、6月翻唱《洋葱》、11月翻唱《董小姐》。\n2013年8月6日，TF家族官方发布TFBOYS形象宣传片《十年》，由王俊凯、王源、易烊千玺三人组成的中国内地组合TFBOYS正式出道，陆续发行单曲《爱出发》、《Heart》、《梦想起航》、《魔法城堡》、《青春修炼手册》、《幸运符号》、《快乐环岛》、《信仰之名》。",
    "country": "中国",
    "source": "web",
    "company": "",
    "aliasname": "",
    "comment_num": 0,
    "collect_num": 0
}
    * */


}
