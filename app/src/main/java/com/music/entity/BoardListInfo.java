package com.music.entity;

import java.io.Serializable;

/**
 * Created by dingfeng on 2016/9/21.
 */
public class BoardListInfo implements Serializable {

    /**
     * #主打榜单
     * 1.新歌榜
     * 2.热歌榜
     * #分类榜单
     * 20.华语金曲榜
     * 21.欧美金曲榜
     * 24.影视金曲榜
     * 23.情歌对唱榜
     * 25.网络歌曲榜
     * 22.经典老歌榜
     * 11.摇滚榜
     * #媒体榜单
     * 6.KTV热歌榜
     * 8.Billboard
     * 18.Hito中文榜
     * 7.叱咤歌曲榜
     */

    private String mTitle;//榜单名
    private String mType;//榜单类型
    private String mCover;//榜单封面
    private String mCoverBig;
    private String mComment;
    private String mMusic1;//榜单第一首
    private String mMusic2;//榜单第二首
    private String mMusic3;//榜单第三首


    public String getTitle() {
        return mTitle;
    }

    public void setTitle(String title) {
        this.mTitle = title;
    }

    public String getType() {
        return mType;
    }

    public void setType(String type) {
        this.mType = type;
    }

    public String getCoverUrl() {
        return mCover;
    }

    public void setCoverUrl(String cover) {
        this.mCover = cover;
    }

    public String getCoverBig() {
        return mCoverBig;
    }

    public void setCoverBig(String coverBig) {
        this.mCoverBig = coverBig;
    }

    public String getComment() {
        return mComment;
    }

    public void setComment(String comment) {
        this.mComment = comment;
    }

    public String getMusic1() {
        return mMusic1;
    }

    public void setMusic1(String music1) {
        this.mMusic1 = music1;
    }

    public String getMusic2() {
        return mMusic2;
    }

    public void setMusic2(String music2) {
        this.mMusic2 = music2;
    }

    public String getMusic3() {
        return mMusic3;
    }

    public void setMusic3(String music3) {
        this.mMusic3 = music3;
    }

}
