package com.music.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.music.application.MusicApplication;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utility {

    public static void showAlert(Context context, String title, String text) {
        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setTitle(title);
        alertBuilder.setMessage(text);
        alertBuilder.create().show();
    }

    private static void mkdirs(File dir_) {
        if (dir_ == null) {
            return;
        }
        if ((dir_.exists()) || (dir_.mkdirs())) return;
        throw new RuntimeException("fail to make " + dir_.getAbsolutePath());
    }

    private static void createNewFile(File file_) {
        if (file_ == null) {
            return;
        }
        if (__createNewFile(file_)) return;
        throw new RuntimeException(file_.getAbsolutePath() + " doesn't be created!");
    }

    private static void delete(File f) {
        if ((f != null) && (f.exists()) && (!f.delete()))
            throw new RuntimeException(f.getAbsolutePath() + " doesn't be deleted!");
    }

    private static boolean __createNewFile(File file_) {
        if (file_ == null) {
            return false;
        }
        makesureParentExist(file_);
        if (file_.exists())
            delete(file_);
        try {
            return file_.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    public static boolean doesExisted(File file) {
        return (file != null) && (file.exists());
    }

    public static boolean doesExisted(String filepath) {
        if (TextUtils.isEmpty(filepath)) return false;
        return doesExisted(new File(filepath));
    }

    private static void makesureParentExist(File file_) {
        if (file_ == null) {
            return;
        }
        File parent = file_.getParentFile();
        if ((parent != null) && (!parent.exists()))
            mkdirs(parent);
    }

    private static void makesureFileExist(File file) {
        if (file == null)
            return;
        if (!file.exists()) {
            makesureParentExist(file);
            createNewFile(file);
        }
    }

    private static void makesureFileExist(String filePath_) {
        if (filePath_ == null)
            return;
        makesureFileExist(new File(filePath_));
    }

    public static boolean isWifi(Context mContext) {
        ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetInfo = connectivityManager.getActiveNetworkInfo();
        return (activeNetInfo != null) && (activeNetInfo.getType() == 1);
    }

    public static boolean isNetworkAvailable(Context ct) {
        ConnectivityManager connectivity = (ConnectivityManager) ct.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            return false;
        }
        NetworkInfo[] info = connectivity.getAllNetworkInfo();
        if (info != null) {
            for (NetworkInfo name : info) {
                if (NetworkInfo.State.CONNECTED == name.getState()) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean isMobileNumber(String mobiles) {
        Pattern p = Pattern.compile("^((13[0-9])|(14[0-9])|(15[0-9])|(16[0-9])|(17[0-9])|(18[0-9]))\\d{8}$");
        Matcher m = p.matcher(mobiles);
        return m.matches();
    }

    public static boolean isEmptyList(List<?> list) {
        if (list == null || list.isEmpty()) {
            return true;
        }
        return false;
    }

    public static int dp2px(float dpValue) {
        final float scale = MusicApplication.getInstance().getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    public static int px2dp(float pxValue) {
        final float scale = MusicApplication.getInstance().getResources().getDisplayMetrics().density;
        return (int) (pxValue / scale + 0.5f);
    }

    public static int sp2px(float spValue) {
        final float fontScale = MusicApplication.getInstance().getResources().getDisplayMetrics().scaledDensity;
        return (int) (spValue * fontScale + 0.5f);
    }

}