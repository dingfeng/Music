package com.music.utils;

import android.widget.Toast;

import com.music.application.MusicApplication;

/**
 * Created by dingfeng on 2015/12/11.
 */
public class ToastUtil {
    private static Toast toast;

    public static void showToast(String text) {
        if (toast == null) {
            toast = Toast.makeText(MusicApplication.getInstance(), text, Toast.LENGTH_SHORT);
        } else {
            toast.setText(text);
            toast.setDuration(Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    public static void showToast(int textId) {
        if (toast == null) {
            toast = Toast.makeText(MusicApplication.getInstance(), textId, Toast.LENGTH_SHORT);
        } else {
            toast.setText(textId);
            toast.setDuration(Toast.LENGTH_SHORT);
        }
        toast.show();
    }

    public static void cancelToast() {
        if (toast != null) {
            toast.cancel();
        }
    }

}
