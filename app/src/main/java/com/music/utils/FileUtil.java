package com.music.utils;

import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by dingfeng on 2016/10/8.
 */
public class FileUtil {

    public static void saveLrcFile(String path, String content) {
        try {
            FileWriter writer = new FileWriter(path);
            writer.flush();
            writer.write(content);
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
