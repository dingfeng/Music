package com.music.application;

import android.app.Application;

import com.lzy.okgo.OkGo;
import com.music.utils.ScreenUtil;

/**
 * Created by dingfeng on 2016/4/12.
 */
public class MusicApplication extends Application {

    private static MusicApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        AppInfo.init(mInstance);
        //必须调用初始化
        OkGo.init(this);

        ScreenUtil.init(this);
    }

    public static MusicApplication getInstance() {
        return mInstance;
    }

}
