package com.music.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.music.R;
import com.music.setting.ScanTask;
import com.music.ui.view.ScanAnimation;

/**
 * Created by dingfeng on 2016/4/16.
 */
public class ScanActivity extends BaseActivity implements View.OnClickListener {

    private RelativeLayout back;
    private TextView txtTitle;
    private TextView txtResult;
    private Button btScan;
    private Button btScanEnd;
    private ScanAnimation scanAnimation;

    @Override
    protected void onCreate(Bundle bundle) {
        // TODO Auto-generated method stub
        super.onCreate(bundle);
        setContentView(R.layout.activity_scan);
        back = (RelativeLayout) findViewById(R.id.back);
        back.setOnClickListener(backClickListener);
        txtTitle = (TextView) findViewById(R.id.txtTitle);
        txtTitle.setText(R.string.music_scan_title);
        txtResult = (TextView) findViewById(R.id.txtResult);
        btScan = (Button) findViewById(R.id.btScan);
        btScan.setOnClickListener(this);
        btScanEnd = (Button) findViewById(R.id.btScanEnd);
        btScanEnd.setOnClickListener(this);

        scanAnimation = (ScanAnimation) findViewById(R.id.scan_animation_view);
        scanAnimation.setWillNotDraw(false);
    }

    @Override
    public void onClick(View view) {
        // TODO Auto-generated method stub
        if (view.getId() == R.id.btScan) {
            new ScanTask(ScanActivity.this, callback).execute();
        } else if (view.getId() == R.id.btScanEnd) {
            onBackPressed();
        }
    }

    ScanTask.Callback callback = new ScanTask.Callback() {
        @Override
        public void start() {
            btScan.setClickable(false);
            btScan.setText(R.string.scaning);
            scanAnimation.setSearching(true);
        }

        @Override
        public void scan() {

        }

        @Override
        public void end(int result) {
            btScan.setVisibility(View.GONE);
            btScanEnd.setVisibility(View.VISIBLE);
            txtResult.setVisibility(View.VISIBLE);
            txtResult.setText(getString(R.string.scan_count, result));
            scanAnimation.setSearching(false);
        }

    };
}

