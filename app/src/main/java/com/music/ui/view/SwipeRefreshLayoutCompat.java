package com.music.ui.view;

import android.content.Context;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewConfiguration;

/**
 * Created by dingfeng on 2016/8/18.
 */
public class SwipeRefreshLayoutCompat extends SwipeRefreshLayout {

    private int mTouchSlop;
    private float mLastX;
    private float mLastY;

    public SwipeRefreshLayoutCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        mTouchSlop = ViewConfiguration.get(context).getScaledTouchSlop();
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                mLastX = event.getX();
                mLastY = event.getY();
                break;
            case MotionEvent.ACTION_MOVE:
                final float eventX = event.getX();
                final float eventY = event.getY();
                float xDiff = Math.abs(eventX - mLastX);
                float yDiff = Math.abs(eventY - mLastY);
                if (xDiff > mTouchSlop || xDiff > yDiff) {
                    return false;
                }
        }
        return super.onInterceptTouchEvent(event);
    }

}
