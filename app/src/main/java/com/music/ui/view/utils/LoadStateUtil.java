package com.music.ui.view.utils;

import android.view.View;

/**
 * Created by dingfeng on 2016/9/27.
 */
public class LoadStateUtil {

    public static void stateChange(View loadSuccess, View loading, View loadFail, StateEnum state) {
        switch (state) {
            case LOADING:
                loadSuccess.setVisibility(View.GONE);
                loading.setVisibility(View.VISIBLE);
                loadFail.setVisibility(View.GONE);
                break;
            case LOAD_SUCCESS:
                loadSuccess.setVisibility(View.VISIBLE);
                loading.setVisibility(View.GONE);
                loadFail.setVisibility(View.GONE);
                break;
            case LOAD_FAIL:
                loadSuccess.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
                loadFail.setVisibility(View.VISIBLE);
                break;
        }
    }

    public static void stateChange(View loadSuccess, View empty, StateEnum state) {
        switch (state) {
            case LOAD_SUCCESS:
                loadSuccess.setVisibility(View.VISIBLE);
                empty.setVisibility(View.GONE);
                break;
            case EMPTY:
                loadSuccess.setVisibility(View.GONE);
                empty.setVisibility(View.VISIBLE);
                break;
        }
    }

}
