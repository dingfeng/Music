package com.music.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

/**
 * Created by dingfeng on 2015/12/16.
 */
public abstract class BaseListAdapter<E> extends BaseAdapter {

    protected List<E> mList;
    protected Context mContext;
    protected LayoutInflater mInflater;

    public BaseListAdapter(Context context, List<E> list) {
        super();
        mContext = context;
        mList = list;
        mInflater = LayoutInflater.from(context);
    }

    public List<E> getList() {
        return mList;
    }

    public void setList(List<E> list) {
        mList = list;
    }

    public void add(E e) {
        mList.add(e);
        notifyDataSetChanged();
    }

    public void addAll(List<E> list) {
        mList.addAll(list);
        notifyDataSetChanged();
    }

    public void remove(int position) {
        mList.remove(position);
        notifyDataSetChanged();
    }

    public void removeAll() {
        mList.clear();
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = bindView(position, convertView, parent);
        return convertView;
    }

    public abstract View bindView(int position, View convertView, ViewGroup parent);
}
