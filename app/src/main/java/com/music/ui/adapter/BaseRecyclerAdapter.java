package com.music.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2015/12/17.
 */
public abstract class BaseRecyclerAdapter<T, VH extends BaseRecyclerAdapter.BaseViewHolder> extends Adapter<VH> {

    protected Context mContext;
    protected List<T> mList;
    protected LayoutInflater mInflater;

    public BaseRecyclerAdapter(Context context) {
        super();
        mContext = context;
        mList = new ArrayList<T>();
    }

    public BaseRecyclerAdapter(Context context, List<T> list) {
        super();
        this.mContext = context;
        this.mList = list;
    }

    public List<T> getList() {
        return mList;
    }

    public void setList(List<T> list) {
        this.mList = list;
    }

    public void add(T t) {
        mList.add(t);
    }

    public void addAll(List<T> list) {
        mList.addAll(list);
    }

    public void remove(int position) {
        mList.remove(position);
    }

    @Override
    public int getItemCount() {
        // TODO Auto-generated method stub
        return mList.size();
    }

    @Override
    public void onBindViewHolder(final VH holder, final int position) {
        // TODO Auto-generated method stub
        if (mOnItemClickListener != null) {
            holder.itemView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
//                    int pos = holder.getPosition();
                    mOnItemClickListener.onItemClick(holder.itemView, position);
                }
            });

            holder.itemView.setOnLongClickListener(new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
//                    int pos = holder.getPosition();
                    mOnItemClickListener.onItemLongClick(holder.itemView, position);
                    return false;
                }
            });
        }
    }

    public static class BaseViewHolder extends ViewHolder {

        public BaseViewHolder(View itemView) {
            super(itemView);
            // TODO Auto-generated constructor stub
        }

    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
        void onItemLongClick(View view, int position);
    }

    private OnItemClickListener mOnItemClickListener;

    public void setOnItemClickListener(OnItemClickListener mOnItemClickListener) {
        this.mOnItemClickListener = mOnItemClickListener;
    }
}
