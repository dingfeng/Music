package com.music.ui.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.music.R;
import com.music.entity.BoardInfo;
import com.music.entity.BoardListInfo;
import com.music.entity.online.OnlineMusicInfo;
import com.music.loader.Api;
import com.music.loader.DataRequest;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dingfeng on 2016/9/21.
 */
public class BoardListAdapter extends BaseAdapter {

    private Context mContext;
    private List<BoardListInfo> mList = new ArrayList<>();

    public BoardListAdapter(Context context, List<BoardListInfo> list) {
        mContext = context;
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final BoardListInfo item = mList.get(position);
        final ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.item_board_list, null);
            holder = new ViewHolder();
            holder.iv_cover = (ImageView) convertView.findViewById(R.id.iv_cover);
            holder.txtBoard = (TextView) convertView.findViewById(R.id.txtBoard);
            holder.tv_music_1 = (TextView) convertView.findViewById(R.id.tv_music_1);
            holder.tv_music_2 = (TextView) convertView.findViewById(R.id.tv_music_2);
            holder.tv_music_3 = (TextView) convertView.findViewById(R.id.tv_music_3);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (TextUtils.isEmpty(item.getCoverUrl())) {
            DataRequest.getInstance().getBillboard(Api.METHOD_GET_MUSIC_LIST, item.getType(), 3, new DataRequest.Callback<BoardInfo>() {
                @Override
                public void onSuccess(BoardInfo data) {
                    if (data == null) {
                        return;
                    }
                    BoardInfo.Billboard billboard = data.getBillboard();
                    List<OnlineMusicInfo> musicList = data.getOnlineMusicList();
                    item.setCoverUrl(billboard.getPics260());
                    item.setCoverBig(billboard.getPics444());
                    item.setTitle((billboard.getName()));
                    item.setComment(billboard.getComment());
                    if (musicList.size() > 2) {
                        item.setMusic1(mContext.getString(R.string.board_list_item_title_1, musicList.get(0).getTitle(), musicList.get(0).getArtistName()));
                        item.setMusic2(mContext.getString(R.string.board_list_item_title_2, musicList.get(1).getTitle(), musicList.get(1).getArtistName()));
                        item.setMusic3(mContext.getString(R.string.board_list_item_title_3, musicList.get(2).getTitle(), musicList.get(2).getArtistName()));
                    } else if (musicList.size() > 1) {
                        item.setMusic1(mContext.getString(R.string.board_list_item_title_1, musicList.get(0).getTitle(), musicList.get(0).getArtistName()));
                        item.setMusic2(mContext.getString(R.string.board_list_item_title_2, musicList.get(1).getTitle(), musicList.get(1).getArtistName()));
                        item.setMusic3("");
                    } else if (musicList.size() > 0) {
                        item.setMusic1(mContext.getString(R.string.board_list_item_title_1, musicList.get(0).getTitle(), musicList.get(0).getArtistName()));
                        item.setMusic2("");
                        item.setMusic3("");
                    } else {
                        item.setMusic1("");
                        item.setMusic2("");
                        item.setMusic3("");
                    }
                    bindData(item, holder);
                }

                @Override
                public void onError(String err) {

                }

            });
        } else {
            bindData(item, holder);
        }

        return convertView;
    }

    private void bindData(final BoardListInfo boardListInfo, ViewHolder holder) {
        Glide.with(mContext)
                .load(boardListInfo.getCoverUrl())
                .placeholder(R.drawable.default_board_cover)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.iv_cover);
        holder.txtBoard.setText(boardListInfo.getTitle());
        holder.tv_music_1.setText(boardListInfo.getMusic1());
        holder.tv_music_2.setText(boardListInfo.getMusic2());
        holder.tv_music_3.setText(boardListInfo.getMusic3());
    }

    class ViewHolder {
        ImageView iv_cover;
        TextView txtBoard;
        TextView tv_music_1;
        TextView tv_music_2;
        TextView tv_music_3;
    }

}
