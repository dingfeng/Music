package com.music.ui.fragment;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.music.R;
import com.music.ui.activity.MediaPlaybackActivity;
import com.music.entity.AlbumInfo;
import com.music.entity.MusicInfo;
import com.music.service.MusicUtil;
import com.music.sqlite.DBUtil;
import com.music.utils.CoverLoader;
import com.music.utils.ImageUtil;
import com.music.utils.ScreenUtil;
import com.music.ui.view.AlbumCoverView;

/**
 * Created by dingfeng on 2016/10/8.
 */
public class PlayFragment extends Fragment {

    private AlbumCoverView albumCover;
    private MusicInfo currentmusic;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        Intent intent = getActivity().getIntent();
        currentmusic = intent.getParcelableExtra("current");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_play, container, false);
        initView(view);
        loadCover();
        return view;
    }

    private void initView(View view) {
        albumCover = (AlbumCoverView) view.findViewById(R.id.albumCover);
        try {
            albumCover.initNeedle(MusicUtil.sService.isPlaying());
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    private void loadCover() {
        if (!currentmusic.isOnline()) {
            int albumId = currentmusic.getAlbumId();
            AlbumInfo albuminfo = DBUtil.getInstance().getAlbumInfoDao().query("albumid", albumId);
            if (albuminfo != null) {
                albumCover.setCoverBitmap(CoverLoader.getInstance().loadRound(albuminfo.getAlbumCover()));
            }
        } else {
            getCover();
        }
    }

    private void getCover() {
        SimpleTarget simpleTarget = new SimpleTarget<Bitmap>(ScreenUtil.getScreenWidth() / 2, ScreenUtil.getScreenWidth() / 2) {
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                Bitmap cover = ImageUtil.resizeImage(resource, ScreenUtil.getScreenWidth() / 2, ScreenUtil.getScreenWidth() / 2);
                cover = ImageUtil.createCircleImage(cover);
                albumCover.setCoverBitmap(cover);
                // set bg
                Bitmap bg = ImageUtil.blur(resource, ImageUtil.BLUR_RADIUS);
                ((MediaPlaybackActivity)getActivity()).setBg(bg);
            }
        };
        Glide.with(this)
                .load(currentmusic.getCover())
                .asBitmap()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(simpleTarget);
    }

    @Override
    public void onResume() {
        super.onResume();
        try {
            if (MusicUtil.sService.isPlaying()) {
                albumCover.start();
            } else {
                albumCover.pause();
            }
        } catch (RemoteException e) {
            e.printStackTrace();
        }
    }

    public void pause() {
        albumCover.pause();
    }

    public void start() {
        albumCover.start();
    }

    public void onChange(MusicInfo music) {
        currentmusic = music;
        loadCover();
    }

}

