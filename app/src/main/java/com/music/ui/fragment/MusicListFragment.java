package com.music.ui.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.music.R;
import com.music.ui.adapter.MusicListAdapter;
import com.music.entity.MusicInfo;
import com.music.sqlite.DBUtil;

import java.util.List;

/**
 * Created by dingfeng on 2016/4/15.
 */
public class MusicListFragment extends FragmentBase
        implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    private ListView mListView;
    private MusicListAdapter mAdapter;
    private List<MusicInfo> mMusicList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.music_list, container, false);
        initView(view);
        bindData();
        return view;
    }

    private void initView(View view) {
        mListView = (ListView) view.findViewById(R.id.listview);
    }

    private void bindData() {
        mMusicList = DBUtil.getInstance().getMusicInfoDao().queryAll();
        if (mMusicList != null) {
            mAdapter = new MusicListAdapter(getActivity(), mMusicList);
            mListView.setAdapter(mAdapter);
            mListView.setOnItemClickListener(this);
            mListView.setOnItemLongClickListener(this);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO Auto-generated method stub
        mAdapter.onItemClick(position);
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        mAdapter.onItemLongClick(position);
        return true;
    }

}

