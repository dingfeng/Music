package com.music.ui.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.music.R;
import com.music.entity.MusicInfo;

/**
 * Created by dingfeng on 2016/4/16.
 */
public class RhythmFragment extends Fragment {

    private Context mContext;
    private FrameLayout flEmpty;

    private MusicInfo currentmusic;
    private String musicname;
    private String artist;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        mContext = getActivity();
        Intent intent = getActivity().getIntent();
        currentmusic = intent.getParcelableExtra("current");
        if (currentmusic != null) {
            musicname = currentmusic.getMusicName();
            artist = currentmusic.getArtist();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        View view = inflater.inflate(R.layout.fragment_rhythm, container, false);
        return view;
    }

}
